package com.wifi.io.mirror.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import com.wifi.io.mirror.util.UIHelper;

public class DvActivity extends Activity implements UIHelper{
	private ProgressDialog mProgressDialog;
	private static SharedPreferences mPreferences;

	
	public SharedPreferences IddPreferences(){
		if(mPreferences == null){
			mPreferences = this.getSharedPreferences("IDD1666_VALUES",Activity.MODE_PRIVATE);
		}
		return mPreferences;
	}
	
	@Override
	public void InitUI() {

	}

	@Override
	public void RequestData() {
	}
	
	@Override
	public void UpdateUI(String key) {
	}

	@Override
	public final void PopupWarning(String message) {
		new AlertDialog.Builder(this).setTitle("Notice")
		.setMessage(message)
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
			           public void onClick(DialogInterface dialog, int which) {}
			       }).show();
		
	}
	
	@Override
	public void PopupError(String message) {
		new AlertDialog.Builder(this).setTitle("Notice")
		.setMessage(message)
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
			           public void onClick(DialogInterface dialog, int which) {}
			       }).show();
		
	}
	
	@Override
	public final void PopupError(Exception e) {
		new AlertDialog.Builder(this).setTitle("Error")
		.setMessage(e.toString())
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
			           public void onClick(DialogInterface dialog, int which) {}
			       }).show();
		
	}
	

	@Override
	public final Context getContext() {
		return this;
	}

	@Override
	public final void PopupWaiting() {
	}

	@Override
	public final void HideWaiting() {
		if(mProgressDialog == null) return;
		mProgressDialog.hide();
		mProgressDialog = null;
	}
}
