package com.wifi.io.mirror.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.wifi.io.mirror.util.StringUtil;



public class DvAsyncTask extends AsyncTask<String, Integer, InputStream> {
	DvActivity ui;
	String key;
	String strData = null;
	public static DvAsyncTask CreateTask(DvActivity ui, String key){
		DvAsyncTask task = new DvAsyncTask();
		task.ui = ui;
		task.key = key;
		return task;
	}
	
	protected void onPreExecute() { 

	}  
			
	protected InputStream doInBackground(String... params) {
		String url = "http://113.11.197.191/message.php?message=";
		return httpGet(url + params[0]);
	}
	
	protected void onPostExecute(InputStream responseStream) {
		if(ui == null) return;
		try {
			String message = StringUtil.getByInputStream(responseStream);
			Log.d("dv", "async task message " + message);
			ui.UpdateUI(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private InputStream httpGet(String url){
		HttpClient iniClient = new DefaultHttpClient();
		HttpResponse response = null;
		InputStream is = null;
		HttpGet get = new HttpGet(url);
		try {
			response = iniClient.execute(get);
			is = response.getEntity().getContent();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return is;
	}
}
