package com.wifi.io.mirror.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.wifi.io.mirror.R;
import com.wifi.io.mirror.ui.MessageBox;
import com.wifi.io.mirror.util.DataCenter;
import com.wifi.io.mirror.util.JsonUtil;
import com.wifi.io.mirror.util.NetUtil;
import com.wifi.io.mirror.util.StringUtil;

@SuppressLint("Wakelock")
public class SearchService extends Service{
	public Context context;
	private static int count = 0;
	private boolean lastStatus = false; 
	private static String URL_OF_STATUS = "http://memo.wifi.io/status.php?type=2";
	NotificationManager notificationManager;
	WakeLock wl;
	
	private boolean personStatuUpdateFlag = false; 
	private MyBinder myBinder = new MyBinder();
	
	@Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }
    
    public class MyBinder extends Binder{
        
        public SearchService getService(){
            return SearchService.this;
        }
    }
    
    public void MyMethod(){
    	personStatuUpdateFlag = true;
        Log.i("dv", "BindService-->MyMethod()");
    }
    
	
	@Override
	public void onCreate() {
		super.onCreate();
		context = this;	
		notificationManager = (NotificationManager)this.getSystemService(NOTIFICATION_SERVICE);
        
		new Thread(new Runnable(){
			public void run(){
				while(true){
					try{
						Thread.sleep(5000);
					}catch(InterruptedException e){
						
					}
					System.out.println("i");
					if(NetUtil.checkNet(context)){
						InputStream is = httpGet(URL_OF_STATUS);
						String result = StringUtil.getByInputStream(is);
						Log.d("dv", result);
						boolean status = JsonUtil.getStatus(result);
						if(isPersonCome(status)){
							Notice();
							//Sound();
							//Vibrate();
						}
						lastStatus = status;
						DataCenter.getCenter().setStatus(status);
						DataCenter.getCenter().setNetworkStatus(true);
					}
				}
			}
		}).start();
	}
	
	private InputStream httpGet(String url){
		HttpClient iniClient = new DefaultHttpClient();
		HttpResponse response = null;
		InputStream is = null;
		HttpGet get = new HttpGet(url);
		try {
			response = iniClient.execute(get);
			is = response.getEntity().getContent();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return is;
	}
	
	private boolean isPersonCome(boolean status){
		boolean result = false;
		if(status == true && status != lastStatus){
			result = true;
		}
		return result;
	}
	
	private void Notice(){
		notificationManager.cancelAll();
		String title = "Mirror";
		String message = "美女照鏡子啦!!";
		Intent messageIntent = new Intent(this,MessageBox.class);
		messageIntent.putExtra("title", title);
		messageIntent.putExtra("message", message);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				messageIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification notification = new Notification();
		notification.icon = R.drawable.m_icon;
		notification.tickerText = message;
		notification.setLatestEventInfo(SearchService.this,title,message,contentIntent);

		Random random = new Random(System.currentTimeMillis());		
		notificationManager.notify(random.nextInt(), notification);

	}
	
	private void Sound(){
		Notification notification = new Notification();
		String ringName = RingtoneManager.getActualDefaultRingtoneUri(SearchService.this, 
				RingtoneManager.TYPE_NOTIFICATION).toString();
		
		notification.sound = Uri.parse(ringName);
		
		notificationManager.notify(0,notification);
	}
	
	private void Vibrate(){
		Notification notification = new Notification();
		notification.vibrate = new long[] {0, 100, 200, 300};
		notificationManager.notify(0,notification);
	}

}
