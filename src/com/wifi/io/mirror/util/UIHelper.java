package com.wifi.io.mirror.util;

import java.io.InputStream;

import android.content.Context;

public interface UIHelper {
	public void InitUI();
	public void RequestData();
	public void UpdateUI(String key);
	public void PopupWarning(String message);
	public void PopupError(String message);
	public void PopupError(Exception e);
	public Context getContext();

	public void PopupWaiting();
	public void HideWaiting();
	}
