package com.wifi.io.mirror.util;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
	public static String getMessage(String jsonStr){
		JSONObject object = null;
		String message = null;
		try {
			object = new JSONObject(jsonStr);
			message = (String) object.get("message");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public static boolean getStatus(String jsonStr){
		JSONObject object = null;
		boolean result = false;
		try {
			object = new JSONObject(jsonStr);
			result = (Boolean)object.get("status");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static boolean getResult(String jsonStr){
		JSONObject object = null;
		boolean result = false;
		try {
			object = new JSONObject(jsonStr);
			result = (Boolean)object.get("result");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
}
