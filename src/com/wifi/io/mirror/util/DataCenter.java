package com.wifi.io.mirror.util;

public class DataCenter {
	public static DataCenter sharedDataCenter = null;
	
	private boolean exist = false;
	private boolean networkStatus = false;
	
	private DataCenter() {
	}
	
	public static DataCenter getCenter(){
		if(sharedDataCenter == null){
			sharedDataCenter = new DataCenter();
			
		}
		return sharedDataCenter;
	}

	public boolean isExist() {
		return exist;
	}

	public void setStatus(boolean status) {
		this.exist = status;
	}

	public boolean isNetworkGood() {
		return networkStatus;
	}

	public void setNetworkStatus(boolean networkStatus) {
		this.networkStatus = networkStatus;
	}
	
	

}
