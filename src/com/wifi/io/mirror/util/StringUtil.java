package com.wifi.io.mirror.util;

import java.io.IOException;
import java.io.InputStream;

public class StringUtil {
	public static String getByInputStream(InputStream in){ 
	       StringBuffer out = new StringBuffer(); 
	       byte[] b = new byte[4096]; 
	       try{
	    	   for (int n; (n = in.read(b)) != -1;) { 
	   	       	out.append(new String(b, 0, n)); 
	   	       } 
	       } catch (Exception e){
	    	   e.printStackTrace();
	       }
	       return out.toString(); 
	   }
}
