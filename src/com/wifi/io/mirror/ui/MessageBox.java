package com.wifi.io.mirror.ui;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.wifi.io.mirror.R;

public class MessageBox extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setFullScreen(this);
		setContentView(R.layout.activity_messag_box);
		
		NotificationManager notificationManager = (NotificationManager)this.getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
		
		Intent intent = getIntent();
		String title = intent.getStringExtra("title");
        String message = intent.getStringExtra("message");
		
		TextView txtMessage = (TextView)findViewById(R.id.message);
		TextView txtTitle = (TextView)findViewById(R.id.title);
		txtMessage.setText(message);
		txtTitle.setText(title);
		
		Button btn = (Button)findViewById(R.id.cancel);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	public void setFullScreen(Activity activity) {
		// activity.getWindow().setFormat(PixelFormat.TRANSLUCENT);
		 activity.getWindow().setFlags(
		 WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

}
